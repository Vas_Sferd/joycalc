import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalcScreen extends JPanel implements ActionListener {
    private Kernel kernel = new Kernel();
    private JLabel field = new JLabel();

    public CalcScreen() {
        add(field);
        field.setText("0");
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String token = actionEvent.getActionCommand();
        if (token.equals("=")) {
            try {
                field.setText("= " + kernel.evaluate());
            } catch (Exception e) {
                field.setText("Error");
            }
            kernel.clearAll();
        } else {
            if (Character.isDigit(token.charAt(0))) {
                // если нажата цифровая кнопка, добавляем операнд
                double operand = Double.parseDouble(token);
                kernel.addOperand(operand);
            } else {
                // иначе добавляем оператор или функцию
                kernel.addOperator(token);
            }
            String expressionString = kernel.getExpressionString();
            field.setText(expressionString);
        }
    }
}
