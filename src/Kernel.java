import java.util.Stack;

public class Kernel {
    private final Stack<String> expression;

    public Kernel() {
        expression = new Stack<>();
    }

    public void addOperand(double operand) {
        expression.push(Double.toString(operand));
    }

    public void addOperator(String operator) {
        expression.push(operator);
    }

    public void clearAll() {
        expression.clear();
    }

    public void removeLast() {
        expression.pop();
    }

    public String getExpressionString() {
        StringBuilder sb = new StringBuilder();
        for (String s : expression) {
            sb.append(s).append(" ");
        }
        return sb.toString().trim();
    }

    public double evaluate() throws Exception {
        if (expression.isEmpty()) {
            throw new Exception("Empty expression");
        }

        Stack<Double> values = new Stack<>();
        Stack<String> operators = new Stack<>();

        while (!expression.isEmpty()) {
            String token = expression.pop();

            if (isNumber(token)) {
                values.push(Double.parseDouble(token));
            } else if (isOperator(token)) {
                while (!operators.isEmpty() && hasHigherPrecedence(operators.peek(), token)) {
                    values.push(applyOperation(operators.pop(), values.pop(), values.pop()));
                }
                operators.push(token);
            } else if (isFunction(token)) {
                double param;
                param = values.pop();
                values.push(evaluateFunction(token, param));
            }
        }

        while (!operators.isEmpty()) {
            values.push(applyOperation(operators.pop(), values.pop(), values.pop()));
        }

        if (values.size() != 1) {
            throw new Exception("Invalid expression");
        }

        return values.pop();
    }

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isOperator(String token) {
        return token.equals("+") || token.equals("-") || token.equals("*")
                || token.equals("/") || token.equals("^") || token.equals("sqrt")
                || token.equals("sin") || token.equals("cos") || token.equals("tan");
    }

    private boolean isFunction(String token) {
        return token.equals("sqrt") || token.equals("sin") || token.equals("cos") || token.equals("tan");
    }

    private double evaluateFunction(String functionName, double param) throws Exception {
        switch (functionName) {
            case "sqrt":
                return Math.sqrt(param);
            case "sin":
                return Math.sin(param);
            case "cos":
                return Math.cos(param);
            case "tan":
                return Math.tan(param);
            default:
                throw new Exception("Unknown function: " + functionName);
        }
    }

    private boolean hasHigherPrecedence(String op1, String op2) {
        int p1 = getPrecedence(op1);
        int p2 = getPrecedence(op2);

        if (p1 == p2 && (op1.equals("^") || op2.equals("^"))) {
            return false; // right associative
        }

        return p1 >= p2;
    }

    private int getPrecedence(String op) {
        switch (op) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            case "^":
                return 3;
            case "sqrt":
            case "sin":
            case "cos":
            case "tan":
                return 4;
        }
        return -1;
    }

    private double applyOperation(String operator, double b, double a) throws Exception {
        switch (operator) {
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                if (b == 0) {
                    throw new Exception("Division by zero");
                }
                return a / b;
            case "^":
                return Math.pow(a, b);
            default:
                throw new Exception("Unknown operator: " + operator);
        }
    }
}
