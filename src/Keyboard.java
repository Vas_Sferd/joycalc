import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class Keyboard extends JPanel {
    public Keyboard(int rowSize ,String[] keys, ActionListener keyListener) {
        setLayout(new GridLayout(0, rowSize, 5, 5));

        for (String key : keys) {
            JButton button = new JButton(key);
            button.addActionListener(keyListener);

            add(button);
        }
    }
}
