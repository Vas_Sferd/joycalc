import java.awt.*;
import javax.swing.*;

public class JoyCalc {
    public static final String[] key_strings = new String[] {
            //"sin", "cos", "tan", "sqrt",
            "7", "8", "9", "-",
            "4", "5", "6", "+",
            "1", "2", "3", "*",
            "/", "0", ".", "="
    };

    public static void main(String[] args) {
        // Создаем окно
        JFrame frame = new JFrame("JoyCalc");
        frame.setSize(360,540);

        // Выставляем способ расположения элементов по горизонтали для панели
        Container pane = frame.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        // Создаем элементы калькулятора: Экран и клавиатуру
        CalcScreen screen = new CalcScreen();
        Keyboard keyboard = new Keyboard(4, key_strings, screen);

        // Добавляем элементы
        pane.add(screen);
        pane.add(keyboard);

        // Отображаем приложение
        frame.setVisible(true);
    }
 }